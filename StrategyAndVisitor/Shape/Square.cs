﻿namespace StrategyAndVisitor
{
    class Square : Figure
    {
        public int width, height;

        public Square(int x, int y, int width, int height)
            : base(x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override string Export(IVisitor visitor)
        => visitor.Export(this);
    }
}
