﻿namespace StrategyAndVisitor
{
    class Point : Figure
    {
        public int dot;

        public Point(int x, int y, int dot)
            : base(x, y)
        {
            this.dot = dot;
        }

        public override string Export(IVisitor visitor)
        => visitor.Export(this);
    }
}
