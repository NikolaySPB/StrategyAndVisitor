﻿namespace StrategyAndVisitor
{
    class Figure
    {
        public int X { get; }
        public int Y { get; }

        public Figure(int x, int y)
        {
            X = x;
            Y = y;
        }

        public virtual string Export(IVisitor visitor)
        => visitor.Export(this);
    }
}
