﻿namespace StrategyAndVisitor
{
    interface IExport
    {
        string Export(Figure figure);
    }
}
