﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAndVisitor.Strategy
{
    class ConsolePrinter : IOutput
    {
        public void Print(string text)
        {
            Console.WriteLine(text);
        }
    }

}
