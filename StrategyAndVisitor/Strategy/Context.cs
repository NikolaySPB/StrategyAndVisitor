﻿using StrategyAndVisitor.Strategy;
using System;
using System.Collections.Generic;

namespace StrategyAndVisitor
{
    class Context
    {
        private readonly IReadOnlyCollection<Figure> _figures;

        private readonly IOutput _output;

        public Context(IOutput output, params Figure[] figures)
        {
            _output = output ?? throw new ArgumentNullException(paramName: nameof(output));
            _figures = figures ?? throw new ArgumentNullException(paramName: nameof(figures));
        }

        public IExport Strategy { private get; set; }

        public void Execute()
        {
            if (Strategy == null)
            {
                throw new InvalidOperationException($"{nameof(Strategy)} was not set");
            }

            foreach (var figure in _figures)
            {
                _output.Print(Strategy.Export(figure));
            }
        }

    }
}
