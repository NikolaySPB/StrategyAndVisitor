﻿namespace StrategyAndVisitor
{
    class ExportJSON : IExport
    {
        public string Export(Figure figure)
        {
            return $@"{{""X"": {figure.X}, ""Y"": {figure.Y}}}";
        }
    }
}
