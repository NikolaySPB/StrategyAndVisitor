﻿namespace StrategyAndVisitor
{
    class ExportXML : IExport
    {
        public string Export(Figure figure)
        {
            return
$@"<FIGURE>
    <X>{figure.X}<X/>
    <Y>{figure.Y}<Y/>
<FIGURE/>";
        }
    }
}
