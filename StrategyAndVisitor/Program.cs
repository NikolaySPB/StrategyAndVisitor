﻿using StrategyAndVisitor.Strategy;
using System;

namespace StrategyAndVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure figure = new Figure(20, 35);
            Figure point = new Point(15, 30, 10);
            Figure circle = new Circle(30, 50, 10);
            Figure square = new Square(10, 20, 5, 7);

            Console.WriteLine("Visitor\n");

            IVisitor visitor = new VisitorXML();

            Console.WriteLine(figure.Export(visitor));
            Console.WriteLine(point.Export(visitor));
            Console.WriteLine(circle.Export(visitor));
            Console.WriteLine(square.Export(visitor));

            visitor = new VisitorJSON();

            Console.WriteLine(figure.Export(visitor));
            Console.WriteLine(point.Export(visitor));
            Console.WriteLine(circle.Export(visitor));
            Console.WriteLine(square.Export(visitor));

            Console.WriteLine("\nStrategy\n");

            Context context = new Context(new ConsolePrinter(), point, circle, square);

            context.Strategy = new ExportXML();
            context.Execute();

            context.Strategy = new ExportJSON();
            context.Execute();


            Console.ReadLine();
        }
    }
}
