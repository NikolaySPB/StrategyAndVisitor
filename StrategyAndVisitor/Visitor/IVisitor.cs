﻿namespace StrategyAndVisitor
{
    interface IVisitor
    {
        string Export(Figure figure);

        string Export(Point figure);

        string Export(Circle figure);

        string Export(Square figure);

        
    }
}
