﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAndVisitor
{
    class VisitorXML : IVisitor
    {
        public string Export(Figure figure)
        {
            return
$@"<FIGURE>
    <X>{figure.X}<X/>
    <Y>{figure.Y}<Y/>
<FIGURE/>";
        }

        public string Export(Point figure)
        {
            return
$@"<POINT>
    <X>{figure.X}<X/>
    <Y>{figure.Y}<Y/>
    <D>{figure.dot}<D/>
<POINT/>";
        }

        public string Export(Circle figure)
        {
            return
$@"<CIRCLE>
    <X>{figure.X}<X/>
     <Y>{figure.Y}<Y/>
    <R>{figure.radius}<R/>
<CIRCLE/>";
        }

        public string Export(Square figure)
        {
            return
$@"<SQUARE>
    <X>{figure.X}<X/>
    <Y>{figure.Y}<Y/>
    <W>{figure.width}<W/>
    <H>{figure.height}<H/>
<SQUARE/>";
        }       
    }
}
