﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAndVisitor
{
    class VisitorJSON : IVisitor
    {
        public string Export(Figure figure)
        {
            return $@"{{""X"": {figure.X}, ""Y"": {figure.Y}}}";
        }

        public string Export(Point figure)
        {
            return $@"{{""X"": {figure.X}, ""Y"": {figure.Y}, ""D"": {figure.dot}}}";
        }

        public string Export(Circle figure)
        {
            return $@"{{""X"": {figure.X}, ""Y"": {figure.Y}, ""R"": {figure.radius}}}";
        }

        public string Export(Square figure)
        {
            return $@"{{""X"": {figure.X}, ""Y"": {figure.Y}, ""W"": {figure.width}, ""H"": {figure.height}}}";
        }        
    }
}
